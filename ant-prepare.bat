call android update lib-project --path extern/UniversalImageLoader/library

call android update lib-project --path extern/AndroidPinning

call android update lib-project --path extern/MemorizingTrustManager

call android update lib-project --path extern/libsuperuser/libsuperuser

call android update lib-project --path extern/zxing-core

call android update lib-project --path extern/android-support-v4-preferencefragment
md extern\android-support-v4-preferencefragment\libs\
copy libs\android-support-v4.jar extern\android-support-v4-preferencefragment\libs\

call android update lib-project --path extern/Support/v7/appcompat --target android-19
md extern\Support\v7\appcompat\libs\
copy libs\android-support-v4.jar extern\Support\v7\appcompat\libs\
md extern\Support\v7\gridlayout\libs\
copy libs\android-support-v4.jar extern\Support\v7\gridlayout\libs\
call android update lib-project --path extern/Support/v7/mediarouter --target android-19
md extern\Support\v7\mediarouter\libs\
copy libs\android-support-v4.jar extern\Support\v7\mediarouter\libs\

call android update project --path . --name F-Droid

echo Successfully updated the main project.

cd test
call android update test-project --path . --main ..
cd ..

echo Successfully updated the test project.
